import { Container, Row, Col, Card, Button } from 'react-bootstrap';
import { useContext } from 'react';
import { Link } from 'react-router-dom';
import UserContext from '../UserContext.js';

export default function ProductCard(props) {
  const { user } = useContext(UserContext);
  const { _id, productName, description, price, productStocks, isActive, productCategory } = props.productProp;

  return (
    <Col xs={12} md={4} className="mb-3">
      <Card>
        <Card.Body>
          <Card.Title className="mb-3">{productName}</Card.Title>
          <Card.Subtitle>Category:</Card.Subtitle>
          <Card.Text>{productCategory}</Card.Text>

          <Card.Subtitle>Description:</Card.Subtitle>
          <Card.Text>{description}</Card.Text>
              
          <Card.Subtitle>Price:</Card.Subtitle>
          <Card.Text>PhP {price}</Card.Text>

          <Card.Subtitle>Stocks:</Card.Subtitle>
          <Card.Text>{productStocks}</Card.Text>

          <Card.Subtitle>Available:</Card.Subtitle>
          <Card.Text>{isActive ? 'Yes' : 'No'}</Card.Text>

          {user.isAdmin === true ? (
            <>
              <Button variant="dark" as={Link} to={`/products/${_id}`}>
                View
              </Button>
              <Button style={{ marginLeft: '0.5rem' }} variant="dark" as={Link} to={`/editProduct/${_id}`}>
                Edit
              </Button>
              <Button style={{ marginLeft: '0.5rem' }} variant="dark" as={Link} to={`/archiveProduct/${_id}`}>
                Archive / Activate
              </Button>
            </>
          ) : user.isAdmin === false ? (
            <Button variant="dark" as={Link} to={`/products/${_id}`}>
              Details
            </Button>
          ) :  (
            <Button variant="dark" as={Link} to="/login">
              Log in
            </Button>
          )}
        </Card.Body>
      </Card>
    </Col>
  );
}
