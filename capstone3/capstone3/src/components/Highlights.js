import React, { useContext } from 'react';
import UserContext from '../UserContext';
import {Container,Row, Col, Carousel } from 'react-bootstrap';






export default function Highlights() {
  const { user } = useContext(UserContext);

  const isAdmin = user.isAdmin;

  
  if (isAdmin) {
    return null;
  }

  return (
		<Container className = 'mt-5 text'>
			<Row >
				<Col className = "col-12 col-md-8 mx-auto ">
					<Carousel>
					      <Carousel.Item interval={2800}>
					        <img
					          className="d-block w-100"
					          src= "image1.jpg"
					          alt="First slide"
					        />					 
					      </Carousel.Item>

					      <Carousel.Item interval={2800}>
					        <img
					          className="d-block w-100"
					          src="image2.jpg"
					          alt="Second slide"
					        />
					      
					      </Carousel.Item>


					      <Carousel.Item>
					        <img
					          className="d-block w-100"
					          src="image3.jpg"
					          alt="Third slide"
					        />					    
					      </Carousel.Item>
					    </Carousel>
				</Col>
			</Row>
		</Container>
		)
}