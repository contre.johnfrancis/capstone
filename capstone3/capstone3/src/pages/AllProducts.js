


import ProductsCard from '../components/ProductsCard.js';
import { useState, useEffect } from 'react';
import { Container, Row } from 'react-bootstrap';



  export default function AllProducts() {
const [products, setProducts] = useState([]);

  useEffect(() => {
    fetch(`https://batch288contreras.onrender.com/products/allProducts`, {
      method: 'GET',
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
      .then(result => result.json())
      .then(data => {
        setProducts(data);
      });
  }, []);

  return (
    <Container>
      <h1 className="text-center mt-3">Products</h1>
      <Row>
        {products.map(product => (
          <ProductsCard key={product._id} productProp={product} />
        ))}
      </Row>
    </Container>
  );
}
