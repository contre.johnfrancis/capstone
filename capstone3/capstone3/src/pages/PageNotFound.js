import {Link} from 'react-router-dom';
import {Container, Row, Col} from 'react-bootstrap';

export default function PageNotFound(){


	return(

	<div>	
		<Container>
			<Row>
				<Col className='col-12 mt-3' >			
					<h1>Page Not Found</h1>
					<p>Go back to the <Link to='/'>homepage</Link></p>			
				</Col>
			</Row>
	</Container>
	</div>
	)
}