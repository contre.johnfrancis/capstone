import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';


// create root() - assigns the element to be managed by react with its virtual dom
const root = ReactDOM.createRoot(document.getElementById('root'));

// render() - displaying the component/ react element in to the root
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>

);


// const name = 'John Snow';
// const element = <h1>Hello, {name}! </h1>

// root.render(element);