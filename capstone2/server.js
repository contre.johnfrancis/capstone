const express = require('express');
const mongoose = require('mongoose');
const usersRoutes = require('./routes/usersRoutes.js')
const productsRoutes = require('./routes/productsRoutes.js');

const cors = require('cors');

const port = 4000;
const app = express();


// Mongo db connection for practice capstone 2
mongoose.connect("mongodb+srv://admin:admin@batch288contreras.ufrsqam.mongodb.net/Capstone2?retryWrites=true&w=majority",{
		useNewUrlParser:true,
		useUnifiedTopology:true
	});


let db = mongoose.connection;

db.on("error", console.error.bind(console,"Error on connecting the cloud database"))
db.once("open", ()=> console.log('Successfully connected to the cloud database'))


//middlewares
app.use(express.json());
app.use(express.urlencoded({extended:true}))
app.use(cors());


//routes
app.use('/users', usersRoutes);
app.use('/products', productsRoutes);

// console port connection
app.listen(port, () => console.log(`The server is running at port ${port}!`))