const express = require("express");
const usersControllers = require('../controllers/usersControllers.js');
const auth = require("../auth.js")
const router = express.Router();

router.get("/myProfile", auth.verify,usersControllers.getCurrentUser);
router.get("/orderedItems",auth.verify,usersControllers.getOrderedItems)
router.post("/register", usersControllers.registerUser);
router.post("/login", usersControllers.loginUser);
router.get("/getAllOrders", auth.verify,usersControllers.getAllOrders);
router.get("/", auth.verify,usersControllers.getAllProfile);
router.get("/:id", auth.verify, usersControllers.getProfile);
router.post("/orderProducts", auth.verify, usersControllers.orderProducts);
router.patch("/orderPayments", auth.verify, usersControllers.orderPayments);

module.exports = router;