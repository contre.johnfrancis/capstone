const mongoose = require('mongoose');

const productSchema = new mongoose.Schema({
  productName: {
    type: String,
  },
  description: {
    type: String,
  },
  productCategory:{
    type: String,
  },
  price: {
    type: Number,
  },
  isActive: {
    type: Boolean,
    default: true
  },
  createdOn: {
    type: Date,
    default: Date.now
  },
  productStocks: {
    type: Number,
    min:1
  },
  userOrders: [{
    userId: {
      type: String,
    },
    orderId: {
      type: String
    },
    orderDate:{
      type: Date,
      default: Date.now
    }
  }]
});

const Products = mongoose.model('Product', productSchema);

module.exports = Products;
