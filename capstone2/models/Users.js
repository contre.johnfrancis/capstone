const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
  isAdmin: {
    type: Boolean,
    default: false
  },
  email: {
    type: String,
    required: [true, "Email is required"]
  },
  password: {
    type: String,
    required: [true, "Password is required"]
  },
  firstName: {
    type: String,
    required: [true, "First name is required"]
  },
  lastName: {
    type: String,
    required: [true, "Last name is required"]
  },
  address: {
    type: String,
    required: [true, "Address is required"]
  },
  mobileNo: {
    type: String,
    required: [true, "Mobile number is required"]
  },
  orderedProduct: [
    {
      productId: {
        type: String
      },
      quantity: {
        type: Number
      },
      totalAmount: {
        type: Number
      },
      orderPayment:{
        type: Number
      },
      totalchange:{
        type: Number
      },

      orderedDate: {
        type: Date
      },
      isPayed: {
        type: Boolean,
        default: false
      }
    }
  ]
});

const Users = mongoose.model("User", userSchema);

module.exports = Users;
