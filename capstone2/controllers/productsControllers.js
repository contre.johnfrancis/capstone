const Products = require('../models/Products.js')
const auth = require('../auth.js');

//this controller
module.exports.addProducts = (request, response) => {
	const userData = auth.decode(request.headers.authorization);
	let newProducts = new Products({
		productName: request.body.productName,
		description: request.body.description,
		productCategory: request.body.productCategory,
		price: request.body.price,
		isActive: request.body.isActive,
		productStocks: request.body.productStocks
	});
	if (!userData.isAdmin) {
			response.send(`You don't have the authorization to access this route`);
	} else {
		newProducts.save()
		.then(save=>response.send(true))
		.catch(error=>response.send(false));
	}
};



// for retrieving all active products
module.exports.getAvailableProducts = (request, response) => {
  Products.find({ isActive: true },{createdOn:0, userOrders:0,__v:0})
    .select("productName description productCategory price productStocks") //for hiding information to all users
    .then((result) => response.send(result))
    .catch((error) => response.send(error));
};



//get all products()
module.exports.getAllProducts = (request, response) => {
  const userData = auth.decode(request.headers.authorization);
  if (userData.isAdmin) {
    Products.find({})
      .then((result) => response.send(result))
      .catch((error) => response.send(false));
  } else {
    return response.send(false);
  }
};



// update products
module.exports.updateProducts = (request, response) => {
  const userData = auth.decode(request.headers.authorization);
  const productId = request.params.productId;

  let updatedProduct = {
    productName: request.body.productName,
    description: request.body.description,
    productCategory: request.body.productCategory,
    price: request.body.price,
    productStocks: request.body.productStocks
  };

  if (userData.isAdmin) {
    Products.findByIdAndUpdate(productId, updatedProduct)
    .then(result => response.send(true))
    .catch(error => response.send(false));
  }else
  {
  	return response.send(false)
}
};


// getting a single product (admin only)
module.exports.getProduct = (request, response) => {
  const productId = request.params.productId;

  Products.findById(productId)
    .then(result => response.send(result))
    .catch(error => response.send(error));
};

// for archiving product
module.exports.archiveProducts = (request, response) => {
  const userData = auth.decode(request.headers.authorization);
  const productId = request.params.productId;

 let archiveProduct = {
    isActive: request.body.isActive
  };

  if (userData.isAdmin) {
    Products.findByIdAndUpdate(productId, archiveProduct)
    .then(result => response.send(true))
    .catch(error => response.send(false));
  }else
  {
  	return response.send(`You don't have the authorization to access this route`)
}
};


module.exports.activateProducts = (request, response) => {
  const userData = auth.decode(request.headers.authorization);
  const productId = request.params.productId;

 let archiveProduct = {
    isActive: request.body.isActive
  };

  if (userData.isAdmin) {
    Products.findByIdAndUpdate(productId, archiveProduct)
    .then(result => response.send("The product has successfully been activated"))
    .catch(error => response.send(error));
  }else
  {
  	return response.send(`You don't have the authorization to access this route`)
}
};

